cd inside
ocamlc -c inlib.mli inlib.ml
cd ..

# uncommenting the lines below fixes the build
# cp outlib.mli.hidden outlib.mli
# ocamlc -I inside -c outlib.mli
ocamlc -I inside -c outlib.ml

ocamlc -I inside -c packlib.mli
# adding `-I inside` below would also fix the build
ocamlc -pack outlib.cmo -o packlib.cmo
